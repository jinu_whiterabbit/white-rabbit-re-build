(function ($) {
   'use strict';
   var wrg = { 
        init: function () {
            this.scrollListner();
            this.footerScripts();
            this.lazyBackground();
       },
       settings: {
            desktop: 1200,
            tab: 1024,
            mobile: 768,
            scrollClassTrigger: 200,
            windowWidth: $(window).width(),
            windowheight: $(window).height(),
            scrollBarWidth: 0
        },

        resizeListner: function() {
            $(window).on('load resize', function() {
                wrg.settings.windowWidth = $(window).width();
            });
        },

        // ScrollListner
        scrollListner: function() {
            $(window).on('load scroll', function() {
                if ($(window).scrollTop() > wrg.settings.scrollClassTrigger) {
                    $('body').addClass('scrolled');
                } else {
                    $('body').removeClass('scrolled');
                }
            });

            $(window).on('mousewheel DOMMouseScroll', function(event) {
                var wd = event.originalEvent.wheelDelta || -event.originalEvent.detail;
                if (wd < 0) {
                    $('body').removeClass('scrollingUp');
                    $('body').addClass('scrollingDown');
                } else {
                    $('body').removeClass('scrollingDown');
                    $('body').addClass('scrollingUp')
                }
            });
        },

        // Only mobile
        onlyMobile: function() {
            if (wrg.settings.windowWidth < wrg.settings.mobile) {

            }
        },

        // Tabs and largescreens
        onlyTabDesktop: function() {
            if (wrg.settings.windowWidth > wrg.settings.tab) {

            }
        },

        // Only largescreens
        onlyDesktop: function() {
            if (wrg.settings.windowWidth > wrg.settings.desktop) {
                this.scrollBarWidth();
                //console.log(window.innerWidth - document.getElementsByTagName('body')[0].clientWidth);
            }
        },

        // Scrollbarwidth
        scrollBarWidth: function() {
            this.settings.scrollBarWidth = window.innerWidth - document.getElementsByTagName('body')[0].clientWidth;
        },    

        footerScripts: function() {
            var stickyfooter = function() {
                var wh = jQuery(window).height();
                var hh = jQuery('.site-header').outerHeight();
                var fh = jQuery('.site-footer').outerHeight();
                jQuery('.site-content').css('min-height', wh - (hh + fh));
            };
            stickyfooter();
            jQuery(window).resize(function() {
                stickyfooter();
            });
            menuH();
            $(window).resize(function() {
                menuH();
            });

            function menuH() {
                var winH = $(window).outerHeight();
                var vmiddle = $('.vmiddle').outerHeight();
                $('.menu-wrapper').css({ height: winH });
                $('.vmiddle').css({ 'margin-top': (winH / 2) - vmiddle / 2 });
            }
        },

        // lazy load background image 

        lazyBackground: function() {
            var ll = $('.partner-list');
            var lh = []
            var wscroll = 0;
            var wh = $(window).height();

            function update_offsets(){
              $('.partner-list').each(function(){
                var x = $(this).offset().top;
                lh.push(x);
              });
            };

            function lazy() {
              wscroll = $(window).scrollTop();
              for(var i = 0; i < lh.length; i++){
                if(lh[i] <= wscroll + (wh + 200)){
                  $('.partner-list').eq(i).addClass('loaded');
                };
              };
            };

            // Page Load
            update_offsets();
            lazy();

            $(window).on('scroll',function(){
              lazy();
            });
        }
   };
   wrg.init(); 
}(jQuery)); 

// Browser detection
(function ($) {
    $.browserTest = function (a, z) {
        var u = 'unknown', x = 'X', m = function (r, h) {
            for (var i = 0; i < h.length; i = i + 1) {
                r = r.replace(h[i][0], h[i][1]);
            }
            return r;
        }, c = function (i, a, b, c) {
            var r = {
                name: m((a.exec(i) || [u, u])[1], b)
            };
            r[r.name] = true;
            r.version = (c.exec(i) || [x, x, x, x])[3];
            if (r.name.match(/safari/) && r.version > 400) {
                r.version = '2.0';
            }
            if (r.name === 'presto') {
                r.version = ($.browser.version > 9.27) ? 'futhark' : 'linear_b';
            }
            r.versionNumber = parseFloat(r.version, 10) || 0;
            r.versionX = (r.version !== x) ? (r.version + '').substr(0, 1) : x;
            r.className = r.name + r.versionX;
            return r;
        };
        a = (a.match(/Opera|Navigator|Minefield|KHTML|Chrome/) ? m(a, [
            [/(Firefox|MSIE|KHTML,\slike\sGecko|Konqueror)/, ''],
            ['Chrome Safari', 'Chrome'],
            ['KHTML', 'Konqueror'],
            ['Minefield', 'Firefox'],
            ['Navigator', 'Netscape']
        ]) : a).toLowerCase();
        $.browser = $.extend((!z) ? $.browser : {}, c(a, /(camino|chrome|firefox|netscape|konqueror|lynx|msie|opera|safari)/, [], /(camino|chrome|firefox|netscape|netscape6|opera|version|konqueror|lynx|msie|safari)(\/|\s)([a-z0-9\.\+]*?)(\;|dev|rel|\s|$)/));
        $.layout = c(a, /(gecko|konqueror|msie|opera|webkit)/, [
            ['konqueror', 'khtml'],
            ['msie', 'trident'],
            ['opera', 'presto']
        ], /(applewebkit|rv|konqueror|msie)(\:|\/|\s)([a-z0-9\.]*?)(\;|\)|\s)/);
        $.os = {
            name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || [u])[0].replace('sunos', 'solaris')
        };
        if (!z) {
            $('html').addClass([$.os.name, $.browser.name, $.browser.className, $.layout.name, $.layout.className].join(' '));
        }
        if (window.devicePixelRatio >= 2) {
          $('html').addClass('retina');
        }
    };
    $.browserTest(navigator.userAgent);
})(jQuery);


//  menu js 
(function ($) {
    "use strict";
    var WRG;
    WRG = function() {
        function u() {
            return window.innerHeight || document.documentElement.offsetHeight || document.documentElement.clientHeight
        }
        function a() {
            function l() {
                return r = window.pageYOffset, f = r > h ? "down" : "up", h = r, f
            }
            function e(i) {
                return i ? $("body").bind("touchmove", function(n) {
                    n.preventDefault()
                }) : ($("body").unbind("touchmove"), n.addClass("closing"), clearTimeout(o), o = setTimeout(function() {
                    n.removeClass("closing")
                }, 100)), n.toggleClass("open", i), t.attr("tabindex", i ? 0 : -1), setTimeout(function() {
                    $("body").toggleClass("open", i)
                }, 200), !1
            }
            var n = $("body .nav-wrapper"), i = !1, o, t = n.find("li a"), h = 0, r, f;
            t.attr("tabindex", -1), $("#toggle-menu").click(function(n) {
                n.preventDefault(), e(i = !i)
            }), $(document).keyup(function(n) {
                n.keyCode === 27 && e(!1)
            });
            $(".main-navigation").on("click", function() {
                e(i = !1)
            });
            $(window).bind("scroll", function() {
                if (!n.hasClass("open")) {
                    var t = l(), i = window.pageYOffset;
                    i > 200 && t == "down" ? n.addClass("hide-it") : n.removeClass("hide-it"), t == "up" && i > 200 ? n.addClass("solid") : n.removeClass("solid")
                }
            })
        }

        function s(n) {
            t.push(n)
        }
        function g() {
            for (var n = 0; n < t.length; n++)
                t[n].call()
        }
        var t = [], n = navigator.userAgent.match(/(iPad|iPhone|iPod|Android)/g), i = ["iPad", "iPhone", "iPod", "Android"], h = navigator.userAgent.match(/(iPad|iPhone|iPod)/g), r = function() {
            var n = function() {
                var n, i = document.createElement("fake"), t = {WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", MSTransition: "msTransitionEnd", OTransition: "oTransitionEnd otransitionend", transition: "transitionend"};
                for (n in t)
                    if (i.style[n] !== undefined)
                        return t[n]
            }();
            return n && {end: n}
        }();
        return{init: function() {
                window.console || (console = {log: function() {
                    }}), a()
            }, getWindowHeight: u, isTouchDevice: n, iOSDevices: ["iPad", "iPhone"]}
    }(), $(WRG.init)
}(jQuery));